package com.olpi.validator;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

public class ValidatorXML {

    private static final Logger logger = LogManager
            .getLogger(ValidatorXML.class);

    public boolean validate(File xmlFile, File xsdSchema) {
        try {
            SchemaFactory factory = SchemaFactory
                    .newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = factory.newSchema(xsdSchema);

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFile));

        } catch (IOException | SAXException ex) {
            logger.error("File XML does not match schema", ex);
            return false;
        }
        return true;
    }

}
