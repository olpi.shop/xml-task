package com.olpi.domain.enums;

public enum VouchersTagName {

    TOURIST_VOUCHERS("tourist_vouchers"),
    TOURIST_VOUCHER("tourist_voucher"),
    TOUR_ID("tour_id"),
    TOUR_OPERATOR("tour_operator"),
    TOUR_TYPE("tour_type"),   
    COUNTRY("country"),
    TERM("term"),
    DAYS_AMOUNT("days_amount"),
    NIGHTS_AMOUNT("nights_amount"),
    TRANSPORT("transport"),
    HOTEL_CHARACTERISTIC("hotel_characteristic"),
    WEBSITE("website"),
    RATING("rating"),
    STAR("star"),
    FOOD_TYPE("food_type"),
    ROOM_TYPE("room_type"),
    FACILITIES("facilities"),
    FACILITY("facility"),
    COST("cost"),
    IS_TICKET_INCLUDED("is_ticket_included"),
    IS_LIVING_INCLUDED("is_living_included"),
    IS_INSURANCE_INCLUDED("is_insurance_included"),
    IS_VISA_INCLUDED("is_visa_included"),
    PRICE("price");

    String name;

    VouchersTagName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static VouchersTagName getTagName(String element) {
        switch (element) {
        case "tourist_vouchers":
            return TOURIST_VOUCHERS;
        case "tourist_voucher":
            return TOURIST_VOUCHER;
        case "tour_id":
            return TOUR_ID;
        case "tour_operator":
            return TOUR_OPERATOR;
        case "tour_type":
            return TOUR_TYPE;
        case "country":
            return COUNTRY;
        case "term":
            return TERM;
        case "days_amount":
            return DAYS_AMOUNT;
        case "nights_amount":
            return NIGHTS_AMOUNT;
        case "transport":
            return TRANSPORT;
        case "hotel_characteristic":
            return HOTEL_CHARACTERISTIC;
        case "website":
            return WEBSITE;
        case "rating":
            return RATING;
        case "star":
            return STAR;
        case "food_type":
            return FOOD_TYPE;
        case "room_type":
            return ROOM_TYPE;
        case "facilities":
            return FACILITIES;
        case "facility":
            return FACILITY;
        case "cost":
            return COST;
        case "is_ticket_included":
            return IS_TICKET_INCLUDED;
        case "is_living_included":
            return IS_LIVING_INCLUDED;
        case "is_insurance_included":
            return IS_INSURANCE_INCLUDED;
        case "is_visa_included":
            return IS_VISA_INCLUDED;
        case "price":
            return PRICE;
        default:
            throw new EnumConstantNotPresentException(VouchersTagName.class,
                    element);
        }
    }

}
