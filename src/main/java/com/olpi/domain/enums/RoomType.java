package com.olpi.domain.enums;

public enum RoomType {

    SINGLE("Single"),
    DOUBLE("Double"),
    TWIN("Twin"),
    TRIPLE("Triple"),
    FAMILY_4("Family_4"),
    FAMILY_5("Family_5");

    String name;

    RoomType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static RoomType getType(String name) {
        switch (name) {
        case "Single":
            return SINGLE;
        case "Double":
            return DOUBLE;
        case "Twin":
            return TWIN;
        case "Triple":
            return TRIPLE;
        case "Family_4":
            return FAMILY_4;
        case "Family_5":
            return FAMILY_5;
        default:
            throw new EnumConstantNotPresentException(RoomType.class, name);
        }
    }

}
