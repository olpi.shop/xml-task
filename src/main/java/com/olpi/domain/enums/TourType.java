package com.olpi.domain.enums;

public enum TourType {

    MEDICINE("Medicine"),
    WEEKEND("Weekend"),
    FISHING("Fishing"),
    TOURISM("Tourism"),
    REST("Rest"),
    PILGRIMAGE("Pilgrimage");

    String name;

    TourType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static TourType getType(String name) {
        switch (name) {
        case "Medicine":
            return MEDICINE;
        case "Weekend":
            return WEEKEND;
        case "Fishing":
            return FISHING;
        case "Tourism":
            return TOURISM;
        case "Rest":
            return REST;
        case "Pilgrimage":
            return PILGRIMAGE;
        default:
            throw new EnumConstantNotPresentException(TourType.class, name);
        }
    }

}
