package com.olpi.domain.enums;

public enum TransportType {

    AVIA("Avia"),
    RAILWAY("Railway"),
    COACH("Coach"),
    LINER("Liner"),
    CAR("Car");

    String name;

    TransportType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static TransportType getType(String name) {
        switch (name) {
        case "Avia":
            return AVIA;
        case "Railway":
            return RAILWAY;
        case "Coach":
            return COACH;
        case "Liner":
            return LINER;
        case "Car":
            return CAR;
        default:
            throw new EnumConstantNotPresentException(TransportType.class,
                    name);
        }
    }

}
