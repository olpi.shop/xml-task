package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.olpi.domain.enums.RoomType;

public class HotelCharacteristic {

    private String website;
    private double rating;
    private int star;
    private String foodType;
    private RoomType roomType;
    private List<String> facilities;

    public HotelCharacteristic() {
        website = "-";
        facilities = new ArrayList<>();
    }

    public void addFacility(String facility) {
        facilities.add(facility);
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public List<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<String> facilities) {
        this.facilities = facilities;
    }

    @Override
    public int hashCode() {
        return Objects.hash(website, rating, star, foodType, roomType,
                facilities);

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        HotelCharacteristic other = (HotelCharacteristic) obj;

        return (website.equals(other.website) && rating == other.rating
                && star == other.star && foodType.equals(other.foodType)
                && roomType.equals(other.roomType));
    }

    @Override
    public String toString() {
        return "HotelCharacteristic:\n   website = " + website
                + "\n   rating = " + rating + "\n   " + star
                + " star\n   foodType = " + foodType + "\n   roomType = "
                + roomType + "\n   facilities = " + facilities;
    }

}
