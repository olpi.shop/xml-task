package com.olpi.domain;

import java.util.Objects;

public class Term {

    private int daysAmount;
    private int nightsAmount;

    public int getDaysAmount() {
        return daysAmount;
    }

    public void setDaysAmount(int daysAmount) {
        this.daysAmount = daysAmount;
    }

    public int getNightsAmount() {
        return nightsAmount;
    }

    public void setNightsAmount(int nightsAmount) {
        this.nightsAmount = nightsAmount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(daysAmount, nightsAmount);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        Term other = (Term) obj;

        return (daysAmount == other.daysAmount
                && nightsAmount == other.nightsAmount);

    }

    @Override
    public String toString() {
        return "Term {days = " + daysAmount + ", nights = " + nightsAmount
                + "}";
    }

}
