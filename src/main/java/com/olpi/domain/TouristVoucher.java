package com.olpi.domain;

import java.util.Objects;

import com.olpi.domain.enums.TourType;
import com.olpi.domain.enums.TransportType;

public class TouristVoucher {

    private String tourId;
    private String tourOperator;
    private TourType tourType;
    private String country;
    private Term term;
    private TransportType transport;
    private HotelCharacteristic hotelCharacteristic;
    private Cost cost;

    public TouristVoucher() {
        tourOperator = "TezTour";
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getTourOperator() {
        return tourOperator;
    }

    public void setTourOperator(String tourOperator) {
        this.tourOperator = tourOperator;
    }

    public TourType getTourType() {
        return tourType;
    }

    public void setTourType(TourType tourType) {
        this.tourType = tourType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public TransportType getTransport() {
        return transport;
    }

    public void setTransport(TransportType transport) {
        this.transport = transport;
    }

    public HotelCharacteristic getHotelCharacteristic() {
        return hotelCharacteristic;
    }

    public void setHotelCharacteristic(
            HotelCharacteristic hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tourId, tourOperator, tourType, country, term,
                transport, hotelCharacteristic, cost);

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        TouristVoucher other = (TouristVoucher) obj;

        return (tourId.equals(other.tourId)
                && tourOperator.equals(other.tourOperator)
                && tourType.equals(other.tourType)
                && country.equals(other.country) && term.equals(other.term)
                && transport.equals(other.transport)
                && hotelCharacteristic.equals(other.hotelCharacteristic)
                && cost.equals(other.cost));
    }

    @Override
    public String toString() {
        return "TouristVoucher information { \ntourId = " + tourId
                + "\ntourOperator = " + tourOperator + "\ntourType = "
                + tourType + "\nCountry = " + country + "\n" + term
                + "\nTransport = " + transport + "\n"
                + hotelCharacteristic.toString() + "\n" + cost.toString()
                + "\n}";
    }

}
