package com.olpi.domain;

import java.util.Objects;

public class Cost {

    private boolean isTicketIncluded;
    private boolean isLivingIncluded;
    private boolean isInsuranceIncluded;
    private boolean isVisaIncluded;
    private double price;

    public boolean isTicketIncluded() {
        return isTicketIncluded;
    }

    public void setTicketIncluded(boolean isTicketIncluded) {
        this.isTicketIncluded = isTicketIncluded;
    }

    public boolean isLivingIncluded() {
        return isLivingIncluded;
    }

    public void setLivingIncluded(boolean isLivingIncluded) {
        this.isLivingIncluded = isLivingIncluded;
    }

    public boolean isInsuranceIncluded() {
        return isInsuranceIncluded;
    }

    public void setInsuranceIncluded(boolean isInsuranceIncluded) {
        this.isInsuranceIncluded = isInsuranceIncluded;
    }

    public boolean isVisaIncluded() {
        return isVisaIncluded;
    }

    public void setVisaIncluded(boolean isVisaIncluded) {
        this.isVisaIncluded = isVisaIncluded;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isTicketIncluded, isLivingIncluded,
                isTicketIncluded, isVisaIncluded, price);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        Cost other = (Cost) obj;

        return (isInsuranceIncluded == other.isInsuranceIncluded
                && isLivingIncluded == other.isLivingIncluded
                && isTicketIncluded == other.isTicketIncluded
                && price == other.price
                && isVisaIncluded == other.isVisaIncluded);

    }

    @Override
    public String toString() {
        return "Cost:\nIs ticket included? = " + isTicketIncluded
                + "\nIs living included? = " + isLivingIncluded
                + "\nIs insurance included? = " + isInsuranceIncluded
                + "\nIs visa included? = " + isVisaIncluded + "\nTotal price = "
                + price + "$";
    }

}
