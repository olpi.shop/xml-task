package com.olpi.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.olpi.domain.Cost;
import com.olpi.domain.HotelCharacteristic;
import com.olpi.domain.Term;
import com.olpi.domain.TouristVoucher;
import com.olpi.domain.enums.RoomType;
import com.olpi.domain.enums.TourType;
import com.olpi.domain.enums.TransportType;
import com.olpi.domain.enums.VouchersTagName;

public class DOMVaucherParser {

    private static final Logger logger = LogManager
            .getLogger(DOMVaucherParser.class);

    private List<TouristVoucher> vouchers;
    private File xmlFile;

    public static class Builder {

        private File xmlFile;

        public Builder file(File xmlFile) {
            this.xmlFile = xmlFile;
            return this;
        }

        public DOMVaucherParser build() {
            return new DOMVaucherParser(this);
        }
    }

    private DOMVaucherParser(Builder builder) {
        xmlFile = builder.xmlFile;
        vouchers = new ArrayList<>();
        readVouchers();
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    public void readVouchers() {
        try {
            logger.debug("Read vouchers from file with DOM parser.");

            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();

            Element rootTag = document.getDocumentElement();
            NodeList vaucherNodes = rootTag.getElementsByTagName(
                    VouchersTagName.TOURIST_VOUCHER.getName());

            for (int i = 0; i < vaucherNodes.getLength(); i++) {
                Element vaucherElement = (Element) vaucherNodes.item(i);
                TouristVoucher vaucher = readVoucher(vaucherElement);
                vouchers.add(vaucher);
            }
            logger.debug("Vouchers have been read with DOM parser.");

        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.log(Level.ERROR, "DOM parsing error!", e);
        }
    }

    private TouristVoucher readVoucher(Element vaucherElement) {
        TouristVoucher voucher = new TouristVoucher();
        voucher.setTourId(readAttributeTourId(vaucherElement));
        voucher.setTourOperator(readAttributeTourOperator(vaucherElement));

        logger.debug("Reading vaucher with id: {} started!",
                voucher.getTourId());

        voucher.setTourType(TourType.getType(readTextContent(vaucherElement,
                VouchersTagName.TOUR_TYPE.getName())));
        voucher.setCountry(readTextContent(vaucherElement,
                VouchersTagName.COUNTRY.getName()));
        voucher.setTerm(readTerm(vaucherElement));
        voucher.setTransport(TransportType.getType(readTextContent(
                vaucherElement, VouchersTagName.TRANSPORT.getName())));
        voucher.setHotelCharacteristic(readHotelCharacteristic(vaucherElement));
        voucher.setCost(readCost(vaucherElement));

        logger.debug("Reading vaucher with id: {} has completed!",
                voucher.getTourId());
        return voucher;
    }

    private Term readTerm(Element vaucherElement) {
        Term term = new Term();

        Element termElement = (Element) vaucherElement
                .getElementsByTagName(VouchersTagName.TERM.getName()).item(0);

        term.setDaysAmount(Integer.valueOf(readTextContent(termElement,
                VouchersTagName.DAYS_AMOUNT.getName())));
        term.setNightsAmount(Integer.valueOf(readTextContent(termElement,
                VouchersTagName.NIGHTS_AMOUNT.getName())));

        return term;
    }

    private HotelCharacteristic readHotelCharacteristic(
            Element vaucherElement) {
        HotelCharacteristic hotelCharacteristic = new HotelCharacteristic();

        Element hotelElement = (Element) vaucherElement.getElementsByTagName(
                VouchersTagName.HOTEL_CHARACTERISTIC.getName()).item(0);

        hotelCharacteristic.setRating(readAttributeReting(hotelElement));
        hotelCharacteristic.setWebsite(readAttributeWebsite(hotelElement));
        hotelCharacteristic.setStar(Integer.valueOf(
                readTextContent(hotelElement, VouchersTagName.STAR.getName())));
        hotelCharacteristic.setFoodType(readTextContent(hotelElement,
                VouchersTagName.FOOD_TYPE.getName()));
        hotelCharacteristic
                .setRoomType(RoomType.getType(readTextContent(hotelElement,
                        VouchersTagName.ROOM_TYPE.getName())));
        hotelCharacteristic.setFacilities(readFacilities(hotelElement));

        return hotelCharacteristic;
    }

    private Cost readCost(Element vaucherElement) {
        Cost cost = new Cost();

        Element costElement = (Element) vaucherElement
                .getElementsByTagName(VouchersTagName.COST.getName()).item(0);

        cost.setTicketIncluded(Boolean.valueOf(readTextContent(costElement,
                VouchersTagName.IS_TICKET_INCLUDED.getName())));
        cost.setLivingIncluded(Boolean.valueOf(readTextContent(costElement,
                VouchersTagName.IS_LIVING_INCLUDED.getName())));
        cost.setInsuranceIncluded(Boolean.valueOf(readTextContent(costElement,
                VouchersTagName.IS_INSURANCE_INCLUDED.getName())));
        cost.setVisaIncluded(Boolean.valueOf(readTextContent(costElement,
                VouchersTagName.IS_VISA_INCLUDED.getName())));
        cost.setPrice(Double.valueOf(
                readTextContent(costElement, VouchersTagName.PRICE.getName())));

        return cost;
    }

    private String readAttributeTourOperator(Element vaucherElement) {
        if (!vaucherElement
                .getAttribute(VouchersTagName.TOUR_OPERATOR.getName())
                .equals("")) {
            return vaucherElement
                    .getAttribute(VouchersTagName.TOUR_OPERATOR.getName());
        }
        return "TezTour";
    }

    private String readAttributeTourId(Element vaucherElement) {
        return vaucherElement.getAttribute(VouchersTagName.TOUR_ID.getName());
    }

    private String readAttributeWebsite(Element hotelElement) {
        if (!hotelElement.getAttribute(VouchersTagName.WEBSITE.getName())
                .equals("")) {
            return hotelElement.getAttribute(VouchersTagName.WEBSITE.getName());
        }
        return "-";
    }

    private Double readAttributeReting(Element hotelElement) {
        return Double.valueOf(
                hotelElement.getAttribute(VouchersTagName.RATING.getName()));
    }

    private String readTextContent(Element element, String tagName) {
        return element.getElementsByTagName(tagName).item(0).getTextContent();
    }

    private List<String> readFacilities(Element hotelElement) {
        List<String> facilities = new ArrayList<>();

        NodeList facilityNodes = hotelElement
                .getElementsByTagName(VouchersTagName.FACILITY.getName());
        for (int i = 0; i < facilityNodes.getLength(); i++) {
            facilities.add(facilityNodes.item(i).getTextContent());
        }
        return facilities;
    }

}
