package com.olpi.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.olpi.domain.TouristVoucher;

public class SAXVaucherParser {

    private static final Logger logger = LogManager
            .getLogger(SAXVaucherParser.class);

    private List<TouristVoucher> vouchers;
    private VaucherHandler handler;
    private File xmlFile;

    public static class Builder {

        private VaucherHandler handler;
        private File xmlFile;

        public Builder handler(VaucherHandler handler) {
            this.handler = handler;
            return this;
        }

        public Builder file(File xmlFile) {
            this.xmlFile = xmlFile;
            return this;
        }

        public SAXVaucherParser build() {
            return new SAXVaucherParser(this);
        }
    }

    private SAXVaucherParser(Builder builder) {
        xmlFile = builder.xmlFile;
        handler = builder.handler;
        vouchers = new ArrayList<>();
        readVouchers();
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    private void readVouchers() {
        try {
            logger.debug("Read vouchers from file with SAX parser.");

            XMLReader reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(new FileInputStream(xmlFile)));

            vouchers = handler.getVouchers();
            logger.debug("Vouchers have been read with SAX parser.");

        } catch (IOException | SAXException e) {
            logger.log(Level.ERROR, "SAX parsing error!", e);
        }
    }

}
