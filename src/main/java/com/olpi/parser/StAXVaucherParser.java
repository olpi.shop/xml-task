package com.olpi.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.Cost;
import com.olpi.domain.HotelCharacteristic;
import com.olpi.domain.Term;
import com.olpi.domain.TouristVoucher;
import com.olpi.domain.enums.RoomType;
import com.olpi.domain.enums.TourType;
import com.olpi.domain.enums.TransportType;
import com.olpi.domain.enums.VouchersTagName;

public class StAXVaucherParser {

    private static final Logger logger = LogManager
            .getLogger(StAXVaucherParser.class);

    private List<TouristVoucher> vouchers;
    private File xmlFile;

    public static class Builder {

        private File xmlFile;

        public Builder file(File xmlFile) {
            this.xmlFile = xmlFile;
            return this;
        }

        public StAXVaucherParser build() {
            return new StAXVaucherParser(this);
        }
    }

    private StAXVaucherParser(Builder builder) {
        xmlFile = builder.xmlFile;
        vouchers = new ArrayList<>();
        readVouchers();
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    public void readVouchers() {
        try {
            logger.debug("Read vouchers from file with StAX parser.");

            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLStreamReader reader = inputFactory
                    .createXMLStreamReader(new FileInputStream(xmlFile));

            while (reader.hasNext()) {

                if (reader.next() == XMLStreamConstants.START_ELEMENT) {

                    VouchersTagName tagName = VouchersTagName
                            .getTagName(reader.getLocalName());
                    if (tagName.equals(VouchersTagName.TOURIST_VOUCHER)) {
                        TouristVoucher voucher = readVoucher(reader);
                        vouchers.add(voucher);
                    }
                }
            }
            logger.debug("Vouchers have been read with StAX parser.");
        } catch (XMLStreamException | FileNotFoundException ex) {
            logger.log(Level.ERROR, "StAX parsing error!", ex);
        }
    }

    private TouristVoucher readVoucher(XMLStreamReader reader)
            throws XMLStreamException {
        TouristVoucher voucher = new TouristVoucher();

        voucher.setTourId(readAttributeTourId(reader));
        voucher.setTourOperator(readAttributeTourOperator(reader));

        VouchersTagName tagName;
        while (reader.hasNext()) {
            int type = reader.next();

            switch (type) {
            case XMLStreamConstants.START_ELEMENT:

                tagName = VouchersTagName.getTagName(reader.getLocalName());

                switch (tagName) {
                case TOUR_TYPE:
                    voucher.setTourType(
                            TourType.getType(readContentText(reader)));
                    break;
                case COUNTRY:
                    voucher.setCountry(readContentText(reader));
                    break;
                case TERM:
                    voucher.setTerm(readTerm(reader));
                    break;
                case TRANSPORT:
                    voucher.setTransport(
                            TransportType.getType(readContentText(reader)));
                    break;
                case HOTEL_CHARACTERISTIC:
                    voucher.setHotelCharacteristic(
                            readHotelCharacteristic(reader));
                    break;
                case COST:
                    voucher.setCost(readCost(reader));
                    break;
                default:
                    break;
                }
                break;

            case XMLStreamConstants.END_ELEMENT:
                tagName = VouchersTagName.getTagName(reader.getLocalName());

                if (tagName.equals(VouchersTagName.TOURIST_VOUCHER)) {
                    return voucher;
                }
                break;

            default:
                break;
            }
        }
        logger.log(Level.WARN,
                "File invalid! Teg \"tourist_voucher\" does not close!");
        throw new XMLStreamException("Tag \"tourist_voucher\" invalide!");
    }

    private Term readTerm(XMLStreamReader reader) throws XMLStreamException {
        Term term = new Term();

        VouchersTagName tagName;
        while (reader.hasNext()) {
            int type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {

                tagName = VouchersTagName.getTagName(reader.getLocalName());

                if (tagName.equals(VouchersTagName.DAYS_AMOUNT)) {
                    term.setDaysAmount(
                            Integer.parseInt(readContentText(reader)));
                } else if (tagName.equals(VouchersTagName.NIGHTS_AMOUNT)) {
                    term.setNightsAmount(
                            Integer.parseInt(readContentText(reader)));
                }
            } else if (type == XMLStreamConstants.END_ELEMENT) {

                tagName = VouchersTagName.getTagName(reader.getLocalName());

                if (tagName.equals(VouchersTagName.TERM)) {
                    return term;
                }
            }
        }
        logger.log(Level.WARN, "File invalid! Teg \"term\" does not close!");
        throw new XMLStreamException("Tag \"term\" invalide!");
    }

    private HotelCharacteristic readHotelCharacteristic(XMLStreamReader reader)
            throws XMLStreamException {
        HotelCharacteristic hotelCharacteristic = new HotelCharacteristic();
        List<String> facilities = new ArrayList<>();

        hotelCharacteristic.setRating(readAttributeReting(reader));
        hotelCharacteristic.setWebsite(readAttributeWebsite(reader));

        VouchersTagName tagName;
        while (reader.hasNext()) {

            int type = reader.next();

            if (type == XMLStreamConstants.START_ELEMENT) {
                tagName = VouchersTagName.getTagName(reader.getLocalName());

                switch (tagName) {
                case STAR:
                    hotelCharacteristic
                            .setStar(Integer.parseInt(readContentText(reader)));
                    break;
                case FOOD_TYPE:
                    hotelCharacteristic.setFoodType(readContentText(reader));
                    break;
                case ROOM_TYPE:
                    hotelCharacteristic.setRoomType(
                            RoomType.getType(readContentText(reader)));
                    break;
                case FACILITY:
                    facilities.add(readContentText(reader));
                    break;
                default:
                    break;
                }
            } else if (type == XMLStreamConstants.END_ELEMENT) {

                tagName = VouchersTagName.getTagName(reader.getLocalName());

                if (tagName.equals(VouchersTagName.HOTEL_CHARACTERISTIC)) {
                    return hotelCharacteristic;
                } else if (tagName.equals(VouchersTagName.FACILITIES)) {
                    hotelCharacteristic.setFacilities(facilities);
                }
            }
        }
        logger.log(Level.WARN,
                "File invalid! Teg \"hotel_characteristic\" does not close!");
        throw new XMLStreamException("Tag \"hotel_characteristic\" invalide!");
    }

    private Cost readCost(XMLStreamReader reader) throws XMLStreamException {
        Cost cost = new Cost();

        VouchersTagName tagName = null;
        while (reader.hasNext()) {

            int type = reader.next();

            if (type == XMLStreamConstants.START_ELEMENT) {

                tagName = VouchersTagName.getTagName(reader.getLocalName());

                switch (tagName) {
                case IS_TICKET_INCLUDED:
                    cost.setTicketIncluded(
                            Boolean.parseBoolean(readContentText(reader)));
                    break;
                case IS_LIVING_INCLUDED:
                    cost.setLivingIncluded(
                            Boolean.parseBoolean(readContentText(reader)));
                    break;
                case IS_INSURANCE_INCLUDED:
                    cost.setInsuranceIncluded(
                            Boolean.parseBoolean(readContentText(reader)));
                    break;
                case IS_VISA_INCLUDED:
                    cost.setVisaIncluded(
                            Boolean.parseBoolean(readContentText(reader)));
                    break;
                case PRICE:
                    cost.setPrice(Double.parseDouble(readContentText(reader)));
                    break;
                default:
                    break;
                }
            } else if (type == XMLStreamConstants.END_ELEMENT) {
                tagName = VouchersTagName.getTagName(reader.getLocalName());
                if (tagName.equals(VouchersTagName.COST)) {
                    return cost;
                }
            }
        }
        logger.log(Level.WARN, "File invalid! Teg \"cost\" does not close!");
        throw new XMLStreamException("Tag \"cost\" invalide!");
    }

    private String readContentText(XMLStreamReader reader)
            throws XMLStreamException {
        if (reader.hasNext()) {
            int type = reader.next();
            if (type == XMLStreamConstants.CHARACTERS)
                return reader.getText().trim();
        }
        return "";
    }

    private String readAttributeTourId(XMLStreamReader reader) {
        return reader.getAttributeValue(null,
                VouchersTagName.TOUR_ID.getName());
    }

    private String readAttributeTourOperator(XMLStreamReader reader) {
        if (reader.getAttributeValue(null,
                VouchersTagName.TOUR_OPERATOR.getName()) != null) {
            return reader.getAttributeValue(null,
                    VouchersTagName.TOUR_OPERATOR.getName());
        }
        return "TezTour";
    }

    private String readAttributeWebsite(XMLStreamReader reader) {
        if (reader.getAttributeValue(null,
                VouchersTagName.WEBSITE.getName()) != null) {
            return reader.getAttributeValue(null, "website");
        }
        return "-";
    }

    private Double readAttributeReting(XMLStreamReader reader) {
        return Double.parseDouble(reader.getAttributeValue(null,
                VouchersTagName.RATING.getName()));
    }
}
