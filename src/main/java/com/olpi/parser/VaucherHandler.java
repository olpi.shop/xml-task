package com.olpi.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.olpi.domain.Cost;
import com.olpi.domain.HotelCharacteristic;
import com.olpi.domain.Term;
import com.olpi.domain.TouristVoucher;
import com.olpi.domain.enums.RoomType;
import com.olpi.domain.enums.TourType;
import com.olpi.domain.enums.TransportType;
import com.olpi.domain.enums.VouchersTagName;

public class VaucherHandler extends DefaultHandler {

    private static final Logger logger = LogManager
            .getLogger(VaucherHandler.class);

    private List<TouristVoucher> vouchers = new ArrayList<>();
    private TouristVoucher voucher = null;
    private Term term = null;
    private HotelCharacteristic hotelCharacteristic = null;
    private List<String> facilities = new ArrayList<>();
    private Cost cost = null;

    private StringBuilder text;

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        logger.debug("start reading element: {}", localName);
        text = new StringBuilder();

        switch (localName) {

        case "tourist_voucher":
            voucher = new TouristVoucher();
            voucher.setTourId(readAttributeTourId(attributes));
            voucher.setTourOperator(readAttributeTourOperator(attributes));
            break;

        case "term":
            term = new Term();
            break;

        case "hotel_characteristic":
            hotelCharacteristic = new HotelCharacteristic();
            hotelCharacteristic.setWebsite(readAttributeWebsite(attributes));
            hotelCharacteristic.setRating(readAttributeReting(attributes));
            break;

        case "cost":
            cost = new Cost();
            break;

        case "facilities":
            facilities.clear();
            break;

        default:
            break;
        }
    }

    @Override
    public void characters(char[] buffer, int start, int length) {
        text.append(buffer, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        logger.debug("End reading element: {}", localName);

        switch (VouchersTagName.getTagName(localName)) {
        case TOUR_TYPE:
            voucher.setTourType(TourType.getType(text.toString()));
            break;
        case COUNTRY:
            voucher.setCountry(text.toString());
            break;
        case DAYS_AMOUNT:
            term.setDaysAmount(Integer.parseInt(text.toString()));
            break;
        case NIGHTS_AMOUNT:
            term.setNightsAmount(Integer.parseInt(text.toString()));
            break;
        case TERM:
            voucher.setTerm(term);
            term = null;
            break;
        case TRANSPORT:
            voucher.setTransport(TransportType.getType(text.toString()));
            break;
        case STAR:
            hotelCharacteristic.setStar(Integer.parseInt(text.toString()));
            break;
        case FOOD_TYPE:
            hotelCharacteristic.setFoodType(text.toString());
            break;
        case ROOM_TYPE:
            hotelCharacteristic.setRoomType(RoomType.getType(text.toString()));
            break;
        case FACILITY:
            facilities.add(text.toString());
            break;
        case FACILITIES:
            hotelCharacteristic.setFacilities(facilities);
            break;
        case HOTEL_CHARACTERISTIC:
            voucher.setHotelCharacteristic(hotelCharacteristic);
            hotelCharacteristic = null;
            break;
        case IS_TICKET_INCLUDED:
            cost.setTicketIncluded(Boolean.parseBoolean(text.toString()));
            break;
        case IS_LIVING_INCLUDED:
            cost.setLivingIncluded(Boolean.parseBoolean(text.toString()));
            break;
        case IS_INSURANCE_INCLUDED:
            cost.setInsuranceIncluded(Boolean.parseBoolean(text.toString()));
            break;
        case IS_VISA_INCLUDED:
            cost.setVisaIncluded(Boolean.parseBoolean(text.toString()));
            break;
        case PRICE:
            cost.setPrice(Double.parseDouble(text.toString()));
            break;
        case COST:
            voucher.setCost(cost);
            cost = null;
            break;
        case TOURIST_VOUCHER:
            vouchers.add(voucher);
            voucher = null;
            break;
        default:
            break;
        }
    }

    @Override
    public void warning(SAXParseException exception) {
        logger.log(Level.WARN, "Warning on line: {}:{} ",
                exception.getLineNumber(), exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) {
        logger.log(Level.ERROR, "Error on line: {}:{} ",
                exception.getLineNumber(), exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        logger.log(Level.FATAL, "Fatal on line: {}:{} ",
                exception.getLineNumber(), exception.getMessage());
    }

    private String readAttributeTourId(Attributes attributes) {
        return attributes.getValue("tour_id");
    }

    private String readAttributeTourOperator(Attributes attributes) {
        if (attributes.getValue("tour_operator") != null) {
            return attributes.getValue("tour_operator");
        }
        return "TezTour";
    }

    private String readAttributeWebsite(Attributes attributes) {
        if (attributes.getValue("website") != null) {
            return attributes.getValue("website");
        }
        return "-";
    }

    private Double readAttributeReting(Attributes attributes) {
        return Double.parseDouble(attributes.getValue("rating"));
    }

}
