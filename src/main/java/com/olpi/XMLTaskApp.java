package com.olpi;

import java.io.File;
import java.util.List;

import com.olpi.domain.TouristVoucher;
import com.olpi.parser.DOMVaucherParser;
import com.olpi.parser.SAXVaucherParser;
import com.olpi.parser.StAXVaucherParser;
import com.olpi.parser.VaucherHandler;
import com.olpi.validator.ValidatorXML;

public class XMLTaskApp {

    public static void main(String[] args) {

        File xmlFile = new File("./src/main/resources/touristVouchers.xml");
        File xsdSchema = new File(
                "./src/main/resources/touristVouchersSchema.xsd");

        ValidatorXML validator = new ValidatorXML();

        if (validator.validate(xmlFile, xsdSchema)) {

            VaucherHandler handler = new VaucherHandler();
            SAXVaucherParser saxParsing = new SAXVaucherParser.Builder()
                    .file(xmlFile).handler(handler).build();

            List<TouristVoucher> saxVouchers = saxParsing.getVouchers();

            StAXVaucherParser staxParser = new StAXVaucherParser.Builder()
                    .file(xmlFile).build();
            List<TouristVoucher> staxVouchers = staxParser.getVouchers();

            DOMVaucherParser domParser = new DOMVaucherParser.Builder()
                    .file(xmlFile).build();
            List<TouristVoucher> domVouchers = domParser.getVouchers();

            System.out.println(saxVouchers.equals(staxVouchers)
                    && staxVouchers.equals(domVouchers));

            for (TouristVoucher voucher : staxVouchers) {
                System.out.println(voucher.toString());
                System.out.println("------------------------------------");
            }

        } else {
            System.out.println("FileXML does not match schema!");
        }
    }
}
